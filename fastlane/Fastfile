# # Debugging
# 
# ## No authentication parameters were specified. These must be provided in order to authenticate with Google
# 
# Set SUPPLY_JSON_KEY to the path to the key
# 
# ## 500 Internal Server Error
# 
# Ensure the tag is pushed to GitLab and create the release manually (retry isn't possible)
#

require 'net/http'

default_platform(:android)

platform :android do
  desc "Build debug and test APK for screenshots"
  lane :build_and_screengrab do
    gradle(task: "assembleDebug assembleAndroidTest")
    capture_android_screenshots()
  end

  desc "Import translations and generate screenshots if release-ready"
  lane :pre_release do
    gradle(task: "importTranslations importTranslationsForFastlane lint testDebugUnitTest")
    build_and_screengrab()
  end

  desc "Deploy a new version"
  lane :release do |options|
    gradle(task: "clean assembleRelease")
    upload_to_play(options)
    release_to_gitlab(options)
  end

  lane :upload_to_play do |options|
    upload_to_play_store(track: options[:production] ? "production" : "beta",
                         release_status: "draft",
                         apk: "app/build/outputs/apk/release/app-release.apk",
                         skip_upload_images: true,
                         mapping: "app/build/outputs/mapping/release/mapping.txt")
  end

  lane :release_to_gitlab do |options|
    upload_to_gitlab(version_name: options[:version] ? options[:version] : gradle(task: "androidGitVersionName", flags: "--quiet", print_command: false, print_command_output: false).strip,
                     project_id: 30453147,
                     asset_paths: {"TrebleInfo.apk": "app/build/outputs/apk/release/app-release.apk"},
                     token: File.read("../gitlab_token.txt").strip)
  end
end
